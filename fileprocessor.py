import logging
import socket



def process_file(file_path: str, tcp_host: str, tcp_port:int):
  s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  s.connect((tcp_host, tcp_port))
  logging.info("Processing {0}".format(file_path))
  with open(file_path,"r") as f:
      try:
          for line in f:
              s.send(bytearray(line,'UTF-8'))
      except Exception as e:
          print("Error {0}".format(e))
  s.close() 
# 