
from os import close, error, walk
import os
import time 
import configparser
import logging
import fileprocessor
import csv
from datetime import datetime

config = configparser.ConfigParser()
cwd = os.path.dirname(os.path.realpath(__file__))
print(cwd)
logging.basicConfig(level=logging.INFO,
                    format="%(asctime)s %(levelname)s %(message)s",
                    handlers=[
                        logging.FileHandler(cwd + "/{0}-info.log".format(datetime.now().strftime("%Y%m%d"))),
                        logging.StreamHandler()])

class CheckDirectories():
  
  config.read(cwd + "/config.ini","UTF-8")
  print(len(config.items("DEFAULT")))
  
  results = []
  for i in config.get("DEFAULT","FOLDERS").split(','):
      try:
          _, _, filenames = next(walk(i, topdown=False))
          for file in filenames:
            try:
                logging.info("------------------------------------")  
                logging.info("Processing {0}".format(file))
                file_path = i + "/" + file
                
                is_processed = False
                csv_file = csv.reader(open(cwd + "/processed.csv", 'r'))
                for row in csv_file:
                  if file_path == row[1]:
                    is_processed = True
                    break
                
                if is_processed == False:
                    logging.info("##### Processing #####")
                    fileprocessor.process_file(file_path, config.get("DEFAULT","TCP_SOCKET"), config.getint("DEFAULT","TCP_PORT"))
                    results.append([datetime.now(), file_path])
                    
            except Exception as err:
              logging.error("Error {0}".format(err))
              logging.info("---------------------------------")  
      except Exception as e:
            logging.error("Error {0}".format(e))
            
  with open(cwd + "/processed.csv","a", newline="\n") as csvfile:
                  csv.writer(csvfile).writerows(results)
                  csvfile.close()            
  

